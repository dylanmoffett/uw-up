#!/usr/bin/env bash
#
# A script designed to be called from a Bitbucket webhook. For more information
# about the webhook in use on the development machine, see
# https://bitbucket.org/atlassianlabs/webhook-listener

echo ""
echo "********************************************************************************"
echo "$0 $@"
echo "********************************************************************************"
echo ""

projectDir=$(cd ..; basename "$PWD")
DIR=$(dirname "$(readlink -f "$0")")
UPDATED_BRANCH=$1
INCOMING_COMMIT=$2

echo "Updating ${projectDir}..."

# Ensure that we're executing within the project directory
cd $DIR/..

ON_BRANCH=$(git symbolic-ref --short -q HEAD)
LAST_COMMIT=$(git log --format=format:%H | head -n 1)

# Pull the latest code, but only if the working directory branch matches the
# branch to which code was just pushed. Also, only do it if it's a new commit.
# Bitbucket sometimes sends the hook multiple times.
if [[ $ON_BRANCH == $UPDATED_BRANCH && ! $LAST_COMMIT = $INCOMING_COMMIT* ]]; then
    echo "Pulling the latest code on the $ON_BRANCH branch"
    git pull origin $ON_BRANCH

    # DO OTHER STUFF BELOW
else
    echo "Either the branches are different ($ON_BRANCH !== $UPDATED_BRANCH) or "
    echo "We've already pulled this commit ($LAST_COMMIT == $INCOMING_COMMIT*.)."
    echo "Either way, there's nothing to do so nothing was done."
fi

echo "Complete"
exit 0