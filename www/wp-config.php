<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_HOME','https://uw-up.unionwebsterdev.com');
define('WP_SITEURL','https://uw-up.unionwebsterdev.com');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'uw_up');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'union & webster');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Configure FTP to use the public key we've configured
 */
define('FS_METHOD', 'ssh2');
define('FTP_PUBKEY', '/home/wordpress/wp_rsa.pub');
define('FTP_PRIKEY', '/home/wordpress/wp_rsa');
define('FTP_USER', 'wordpress');
define('FTP_PASS', '');
define('FTP_HOST', '127.0.0.1:22');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8%BcG#U_R70_pkSIM@)]g+2-+}%68x?M{QwWjPLp[$U4ptcWZii{o+C{wWA<-bU%');
define('SECURE_AUTH_KEY',  'B}WI,;^b`?C o37-}V=+u&*Ls>7Bv$}P#k^f3^ul7u<?>y`Et?IA1!7`$/nQ9B+r');
define('LOGGED_IN_KEY',    'KdEM2+BV<w<o><zr.zIy$IF~;7*gU+BL(|2)3@oc5@EqBF]}6f}PN=sYyJj/+pNJ');
define('NONCE_KEY',        ') kCT6rvqqPs{ofCy005lxEhBX7@-u@y|$g4QD4 u7:H0JX|D1Hukam5m=~> l5J');
define('AUTH_SALT',        '3T#:67iezgyyY)xjrd$][0xa0BYtJsN!%IbV@u-,Q![VMQqJF>!~ 2=qea& i8+w');
define('SECURE_AUTH_SALT', 'wI7`e{QKd^oS.-~kxSzT ,44<3^6d4f=w94Ud+]Z:VD~Rg<x|<8s[l}>VyC1r|jD');
define('LOGGED_IN_SALT',   'Ur,.DhqPUa|d~r<w`jw!RWqk! y,[4G-8m4so*nvHv|W#TS#UrIh&C8_-d *PD%)');
define('NONCE_SALT',       'nC|np%bfNr,fJU;CWu|A8,|$|Q,BRp_!~q}g|):HTpIlm5Jc !=NDr@FD^QbU1q:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');